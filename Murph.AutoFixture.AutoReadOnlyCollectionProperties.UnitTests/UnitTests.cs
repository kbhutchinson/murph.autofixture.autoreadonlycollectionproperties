using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Murph.AutoFixture.AutoReadOnlyCollectionProperties;
using NUnit.Framework;
using AutoFixture;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties.UnitTests
{
    public class ModelUsingProperty
    {
        public ICollection< int > PropColl { get; }  = new List< int >();
    }

    public class ModelUsingField
    {
        public readonly ICollection< int > FieldColl = new List< int >();
    }

    public class ModelUsingPropertyWithReadOnlyCollectionObject
    {
        public ICollection< int > PropColl { get; }  = new ReadOnlyCollection< int >( new List< int >() );
    }

    public class ModelUsingFieldWithReadOnlyCollectionObject
    {
        public readonly ICollection< int > FieldColl = new ReadOnlyCollection< int >( new List< int >() );
    }

    // dictionaries implement ICollection<KeyValuePair<T,T>> but let's check them specifically
    public class ModelUsingDictionaryProperty
    {
        public IDictionary< string, string > PropDict { get; } = new Dictionary< string, string >();
    }

    public class ModelUsingDictionaryField
    {
        public readonly IDictionary< string, string > FieldDict = new Dictionary< string, string >();
    }

    public class ModelUsingDictionaryPropertyWithReadOnlyCollectionObject
    {
        public IDictionary< string, string > PropDict { get; } = new ReadOnlyDictionary< string, string >( new Dictionary< string, string >() );
    }

    public class ModelUsingDictionaryFieldWithReadOnlyCollectionObject
    {
        public readonly IDictionary< string, string > FieldDict = new ReadOnlyDictionary< string, string >( new Dictionary< string, string >() );
    }


    [TestFixture]
    public static class UnitTestsWithCollections
    {
        [Test]
        public static void FixtureWithoutCustomization_WhenAutoPropertiesEnabled_DoesNotFillReadOnlyCollectionProperty()
        {
            // Arrange

            var fixture = new Fixture
            {
                OmitAutoProperties = false // this should be the default, but let's be explicit
            };

            // Act

            var model = fixture.Create< ModelUsingProperty >();

            // Assert

            Assert.That( model.PropColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void FixtureWithoutCustomization_WhenAutoPropertiesEnabled_DoesNotFillReadOnlyCollectionField()
        {
            // Arrange

            var fixture = new Fixture
            {
                OmitAutoProperties = false // this should be the default, but let's be explicit
            };

            // Act

            var model = fixture.Create< ModelUsingField >();

            // Assert

            Assert.That( model.FieldColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesEnabled_FillsReadOnlyCollectionProperty()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingProperty >();

            // Assert

            Assert.That( model.PropColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( fixture.RepeatCount ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesEnabled_FillsReadOnlyCollectionField()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingField >();

            // Assert

            Assert.That( model.FieldColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( fixture.RepeatCount ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesDisabled_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();
            fixture.OmitAutoProperties = true;

            // Act

            var model = fixture.Create< ModelUsingProperty >();

            // Assert

            Assert.That( model.PropColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesDisabled_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();
            fixture.OmitAutoProperties = true;

            // Act

            var model = fixture.Create< ModelUsingField >();

            // Assert

            Assert.That( model.FieldColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenSpecificPropertyFillingDisabled_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();
            fixture.Customize< ModelUsingProperty >( composer => composer.WithoutEx( m => m.PropColl ) );

            // Act

            var model = fixture.Create< ModelUsingProperty >();

            // Assert

            Assert.That( model.PropColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenSpecificFieldFillingDisabled_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();
            fixture.Customize< ModelUsingField >( composer => composer.WithoutEx( m => m.FieldColl ) );

            // Act

            var model = fixture.Create< ModelUsingField >();

            // Assert

            Assert.That( model.FieldColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenCollectionObjectIsReadOnly_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingPropertyWithReadOnlyCollectionObject >();

            // Assert

            Assert.That( model.PropColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenCollectionObjectIsReadOnly_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingFieldWithReadOnlyCollectionObject >();

            // Assert

            Assert.That( model.FieldColl, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        /// <summary>
        /// Sets up a fixture with the <see cref="AutoReadOnlyCollectionPropertiesCustomization"/>
        /// </summary>
        /// <returns> Customized fixture </returns>
        private static IFixture Fixture()
        {
            return
                new Fixture()
                {
                    OmitAutoProperties = false, // this should be the default, but let's be explicit
                }
                .Customize( new AutoReadOnlyCollectionPropertiesCustomization() );
        }
    }

    [TestFixture]
    public static class UnitTestsWithDictionaries
    {
        [Test]
        public static void FixtureWithoutCustomization_WhenAutoPropertiesEnabled_DoesNotFillReadOnlyDictionaryProperty()
        {
            // Arrange

            var fixture = new Fixture
            {
                OmitAutoProperties = false // this should be the default, but let's be explicit
            };

            // Act

            var model = fixture.Create< ModelUsingDictionaryProperty >();

            // Assert

            Assert.That( model.PropDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void FixtureWithoutCustomization_WhenAutoPropertiesEnabled_DoesNotFillReadOnlyDictionaryField()
        {
            // Arrange

            var fixture = new Fixture
            {
                OmitAutoProperties = false // this should be the default, but let's be explicit
            };

            // Act

            var model = fixture.Create< ModelUsingDictionaryField >();

            // Assert

            Assert.That( model.FieldDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesEnabled_FillsReadOnlyDictionaryProperty()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingDictionaryProperty >();

            // Assert

            Assert.That( model.PropDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( fixture.RepeatCount ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesEnabled_FillsReadOnlyDictionaryField()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingDictionaryField >();

            // Assert

            Assert.That( model.FieldDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( fixture.RepeatCount ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesDisabled_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();
            fixture.OmitAutoProperties = true;

            // Act

            var model = fixture.Create< ModelUsingDictionaryProperty >();

            // Assert

            Assert.That( model.PropDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenAutoPropertiesDisabled_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();
            fixture.OmitAutoProperties = true;

            // Act

            var model = fixture.Create< ModelUsingDictionaryField >();

            // Assert

            Assert.That( model.FieldDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenSpecificPropertyFillingDisabled_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();
            fixture.Customize< ModelUsingDictionaryProperty >( composer => composer.WithoutEx( m => m.PropDict ) );

            // Act

            var model = fixture.Create< ModelUsingDictionaryProperty >();

            // Assert

            Assert.That( model.PropDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenSpecificFieldFillingDisabled_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();
            fixture.Customize< ModelUsingDictionaryField >( composer => composer.WithoutEx( m => m.FieldDict ) );

            // Act

            var model = fixture.Create< ModelUsingDictionaryField >();

            // Assert

            Assert.That( model.FieldDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenCollectionObjectIsReadOnly_DoesNotFillProperty()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingDictionaryPropertyWithReadOnlyCollectionObject >();

            // Assert

            Assert.That( model.PropDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        [Test]
        public static void Customization_WhenCollectionObjectIsReadOnly_DoesNotFillField()
        {
            // Arrange

            var fixture = Fixture();

            // Act

            var model = fixture.Create< ModelUsingDictionaryFieldWithReadOnlyCollectionObject >();

            // Assert

            Assert.That( model.FieldDict, Has.Property( nameof(ICollection< int >.Count) ).EqualTo( 0 ) );
        }

        /// <summary>
        /// Sets up a fixture with the <see cref="AutoReadOnlyCollectionPropertiesCustomization"/>
        /// </summary>
        /// <returns> Customized fixture </returns>
        private static IFixture Fixture()
        {
            return
                new Fixture()
                {
                    OmitAutoProperties = false, // this should be the default, but let's be explicit
                }
                .Customize( new AutoReadOnlyCollectionPropertiesCustomization() );
        }
    }
}
