using System;
using System.Collections.Generic;
using AutoFixture.Kernel;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties
{
    /// <summary>
    /// Behavior that will instruct the fixture to fill <see cref="ICollection{T}"/>-typed properties
    /// and fields that have no setter, by calling the collection's Add method.
    /// </summary>
    public class AutoReadOnlyCollectionPropertiesBehavior : ISpecimenBuilderTransformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AutoReadOnlyCollectionPropertiesBehavior" /> class.
        /// </summary>
        public AutoReadOnlyCollectionPropertiesBehavior( Func< bool > omitAutoProperties )
            : this( omitAutoProperties, new TrueRequestSpecification() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoReadOnlyCollectionPropertiesBehavior" /> class.
        /// </summary>
        /// <param name="omitAutoProperties"> Function that returns whether auto properties should be omitted </param>
        /// <param name="specification"> The specification to embed in the behavior </param>
        public AutoReadOnlyCollectionPropertiesBehavior( Func< bool > omitAutoProperties, IRequestSpecification specification )
        {
            OmitAutoProperties = omitAutoProperties ?? throw new ArgumentNullException( nameof(omitAutoProperties) );
            Specification      = specification      ?? throw new ArgumentNullException( nameof(specification)      );
        }

        /// <summary>
        /// Function that returns whether auto properties should be omitted.
        /// </summary>
        public Func< bool > OmitAutoProperties { get; }

        /// <summary>
        /// Specification that filters properties and fields that should be populated.
        /// </summary>
        public IRequestSpecification Specification { get; }

        /// <summary>
        /// Transforms the given builder to implement auto-collection filling behavior.
        /// </summary>
        /// <param name="builder"> The builder to transform </param>
        /// <returns> The transformed builder </returns>
        public ISpecimenBuilderNode Transform( ISpecimenBuilder builder )
            => new Postprocessor( builder, new AutoReadOnlyCollectionPropertiesCommand( OmitAutoProperties, Specification ) );
    }
}
