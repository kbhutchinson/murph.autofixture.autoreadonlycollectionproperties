using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoFixture.Kernel;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties
{
    /// <summary>
    /// <see cref="ISpecimenCommand"/> that will add element values to an object's read-only
    /// properties and fields whose types implement <see cref="ICollection{T}"/>. Extends the
    /// built-in auto-property functionality of AutoFixture to ICollection&lt;T>-typed members.
    /// </summary>
    public class AutoReadOnlyCollectionPropertiesCommand : ISpecimenCommand
    {
        private const string IsReadOnlyGetterName = "get_" + nameof(ICollection< object >.IsReadOnly);
        private const string ClearMethodName      = nameof(ICollection< object >.Clear);
        private const string AddMethodName        = nameof(ICollection< object >.Add);

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoReadOnlyCollectionPropertiesCommand" /> class.
        /// </summary>
        public AutoReadOnlyCollectionPropertiesCommand( Func< bool > omitAutoProperties )
            : this( omitAutoProperties, new TrueRequestSpecification() )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoReadOnlyCollectionPropertiesCommand" /> class.
        /// </summary>
        public AutoReadOnlyCollectionPropertiesCommand( Func< bool > omitAutoProperties, IRequestSpecification specification )
        {
            OmitAutoProperties = omitAutoProperties ?? throw new ArgumentNullException( nameof(omitAutoProperties) );
            Specification      = specification      ?? throw new ArgumentNullException( nameof(specification)      );
        }

        /// <summary>
        /// Function that returns whether auto properties should be omitted
        /// </summary>
        public Func< bool > OmitAutoProperties { get; }

        /// <summary>
        /// Specification that filters properties and fields that should be populated.
        /// </summary>
        public IRequestSpecification Specification { get; }

        /// <summary>
        /// Fills <see cref="ICollection{T}"/> properties and fields on a specimen that are skipped
        /// by the normal AutoPropertiesCommand because they have no setter.
        /// </summary>
        /// <param name="specimen"> The specimen on which property and field collections will be filled. </param>
        /// <param name="context"> An <see cref="ISpecimenContext"/> that is used to create element values. </param>
        public void Execute( object specimen, ISpecimenContext context )
        {
            if ( specimen == null )
            {
                return;
            }

            if ( context == null )
            {
                throw new ArgumentNullException( nameof(context) );
            }

            if ( OmitAutoProperties() )
            {
                return;
            }

            AddValues( specimen, GetProperties( specimen ), context );
            AddValues( specimen, GetFields( specimen ), context );
        }

        /// <summary>
        /// Performs the actions of generating and adding element values to the specimen's collection properties and fields
        /// </summary>
        /// <param name="specimen"> The specimen on which property and field collections will be filled </param>
        /// <param name="memberInfos"> MemberInfo instances representing the properties or fields to modify </param>
        /// <param name="context"> An <see cref="ISpecimenContext"/> that is used to create element values </param>
        private static void AddValues< TInfo >( object specimen, IEnumerable< TInfo > memberInfos, ISpecimenContext context ) where TInfo : MemberInfo
        {
            var noParams = new object[0];

            foreach ( var memberInfo in memberInfos )
            {
                var memberCollection =
                    ( memberInfo as PropertyInfo )?.GetValue( specimen ) ?? 
                    ( memberInfo as FieldInfo    )?.GetValue( specimen );

                if ( memberCollection != null )
                {
                    var collectionType = memberCollection.GetType();

                    // each implemented ICollection<> type gets its own shot
                    foreach ( var interfaceType in collectionType.GetClosedConstructedTypesOf( typeof(ICollection<>) ) )
                    {
                        // don't try modifying read-only collections
                        var readOnlyGetter = collectionType.GetImplementationMethods( interfaceType, typeof(bool), IsReadOnlyGetterName ).Single();
                        bool isReadOnly = (bool)readOnlyGetter.Invoke( memberCollection, noParams );

                        if ( ! isReadOnly )
                        {
                            // ask the fixture to create a sample collection, and then copy each element into the member collection
                            if ( context.Resolve( memberInfo ) is IEnumerable sampleCollection ) // this will also skip over OmitSpecimen
                            {
                                // clear the collection first
                                var clearMethod = collectionType.GetImplementationMethods( interfaceType, typeof(void), ClearMethodName ).Single();
                                clearMethod.Invoke( memberCollection, noParams );

                                // add new elements
                                var addMethod = collectionType.GetImplementationMethods( interfaceType, typeof(void), AddMethodName ).Single();
                                foreach ( var element in sampleCollection )
                                {
                                    if ( ! ( element is OmitSpecimen ) )
                                    {
                                        addMethod.Invoke( memberCollection, new object[] { element } );
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve read-only <see cref="ICollection{T}"/> properties on the specimen's type
        /// </summary>
        /// <param name="specimen"> Specimen being built </param>
        /// <returns> List of read-only ICollection&lt;T> properties </returns>
        private IEnumerable< PropertyInfo > GetProperties( object specimen )
        {
            Type specimenType = specimen.GetType();

            return
                from pi in specimenType.GetTypeInfo().GetProperties( BindingFlags.Public | BindingFlags.Instance )
                where
                    pi.GetSetMethod() == null
                    &&
                    pi.GetIndexParameters().Length == 0
                    &&
                    pi.PropertyType.GetClosedConstructedTypesOf( typeof(ICollection<>) ).Any()
                    &&
                    Specification.IsSatisfiedBy( pi )
                select pi;
        }

        /// <summary>
        /// Retrieve read-only <see cref="ICollection{T}"/> fields on the specimen's type
        /// </summary>
        /// <param name="specimen"> Specimen being built </param>
        /// <returns> List of read-only ICollection&lt;T> fields </returns>
        private IEnumerable< FieldInfo > GetFields( object specimen )
        {
            Type specimenType = specimen.GetType();

            return
                from fi in specimenType.GetTypeInfo().GetFields( BindingFlags.Public | BindingFlags.Instance )
                where
                    fi.IsInitOnly
                    &&
                    fi.FieldType.GetClosedConstructedTypesOf( typeof(ICollection<>) ).Any()
                    &&
                    Specification.IsSatisfiedBy( fi )
                select fi;
        }

    }
}
