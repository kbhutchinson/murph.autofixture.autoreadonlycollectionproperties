using AutoFixture;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties
{
    /// <summary>
    /// A <see cref="ICustomization"/> that adds <see cref="AutoReadOnlyCollectionPropertiesBehavior"/> to the fixture.
    /// </summary>
    public class AutoReadOnlyCollectionPropertiesCustomization : ICustomization
    {
        /// <summary>
        /// Customize the fixture for setting collections of read-only properties and fields.
        /// </summary>
        /// <param name="fixture"> Fixture to customize </param>
        public void Customize( IFixture fixture )
            => fixture.Behaviors.Add( new AutoReadOnlyCollectionPropertiesBehavior( () => fixture.OmitAutoProperties ) );
    }
}
