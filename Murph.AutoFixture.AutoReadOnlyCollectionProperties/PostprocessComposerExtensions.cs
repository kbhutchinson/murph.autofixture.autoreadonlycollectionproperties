using System;
using System.Linq.Expressions;
using System.Reflection;
using AutoFixture.Dsl;
using AutoFixture.Kernel;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties
{
    /// <summary>
    /// Extension methods for <see cref="IPostprocessComposer{T}"/>
    /// </summary>
    public static class PostprocessComposerExtensions
    {
        /// <summary>
        /// Modifies an <see cref="IPostprocessComposer{T}"/> to skip auto-filling any property or field, unconstrained by limits in the built-in Without().
        /// </summary>
        /// <param name="composer"> The composer to modify </param>
        /// <param name="propertyPicker"> An expression that identifies the property or field to skip </param>
        /// <returns> The modified composer </returns>
        public static IPostprocessComposer< T > WithoutEx< T, TProperty >( this IPostprocessComposer< T > composer, Expression< Func< T, TProperty > > propertyPicker )
        {
            object request = null;

            MemberExpression body = propertyPicker.Body as MemberExpression;
            if ( body == null )
            {
                // cribbed from Ploeh.AutoFixture.Kernel.ExpressionReflector.TryUnwrapConversion
                if ( propertyPicker.Body is UnaryExpression unary )
                {
                    body = unary.Operand as MemberExpression;
                }
            }

            MemberInfo memberInfo = body?.Member;
            if ( memberInfo == null )
            {
                throw new ArgumentException( "The expression's Body is not a MemberExpression. Most likely this is because it does not represent access to a property or field.", nameof(propertyPicker) );
            }

            if ( memberInfo.MemberType == MemberTypes.Property )
            {
                request = memberInfo as PropertyInfo;
            }
            else if ( memberInfo.MemberType == MemberTypes.Field )
            {
                request = memberInfo as FieldInfo;
            }
            else
            {
                throw new ArgumentException( "The expression does not represent access to a property or field.", nameof(propertyPicker) );
            }

            if ( request != null )
            {
                // turn this builder into one that will return OmitSpecimen when the given MemberInfo request is resolved
                return
                    new NodeComposer< T >(
                        new CompositeSpecimenBuilder(
                            new Omitter( new EqualRequestSpecification( request ) ),
                            composer ) );
            }

            return composer;
        }
    }
}
