using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Murph.AutoFixture.AutoReadOnlyCollectionProperties
{
    /// <summary>
    /// Extension methods for types
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Returns a list of closed, constructed types implemented or inherited
        /// by the given type that correspond to the given interface definition.
        /// </summary>
        public static IEnumerable< Type > GetClosedConstructedTypesOf( this Type type, Type interfaceDefinition )
        {
            if ( type == null )
            {
                throw new ArgumentNullException( nameof(type) );
            }

            if ( interfaceDefinition == null )
            {
                throw new ArgumentNullException( nameof(interfaceDefinition) );
            }

            if ( ! ( interfaceDefinition.IsGenericTypeDefinition && interfaceDefinition.IsInterface ) )
            {
                throw new ArgumentException( "Type must be a generic interface type definition", nameof(interfaceDefinition) );
            }

            var interfaces = new List< Type >( type.GetInterfaces() );
            if ( type.IsInterface )
            {
                interfaces.Add( type );
            }

            return
                interfaces
                .Where( i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceDefinition );
        }

        /// <summary>
        /// Returns a list of <see cref="MethodInfo"/> instances that represent the methods in the implementation
        /// type that correspond to the interface methods matching the given return type and method name.
        /// </summary>
        /// <param name="implementationType"> Type with the implementation methods </param>
        /// <param name="interfaceType"> Interface declaring the methods </param>
        /// <param name="methodReturnType"> Type that the method returns </param>
        /// <param name="methodName"> Name of the method </param>
        /// <returns> List of MethodInfo instances, which may be empty if there are no matches </returns>
        public static IEnumerable< MethodInfo > GetImplementationMethods( this Type implementationType, Type interfaceType, Type methodReturnType, string methodName )
        {
            if ( implementationType == null )
            {
                throw new ArgumentNullException( nameof(implementationType) );
            }

            if ( interfaceType == null )
            {
                throw new ArgumentNullException( nameof(interfaceType) );
            }

            if ( methodReturnType == null )
            {
                throw new ArgumentNullException( nameof(methodReturnType) );
            }

            var map = implementationType.GetInterfaceMap( interfaceType );

            return
                map
                .InterfaceMethods
                .Zip( map.TargetMethods, ( im, tm ) => Tuple.Create( im, tm ) )
                .Where( t => t.Item1.ReturnType == methodReturnType && t.Item1.Name.Equals( methodName ) )
                .Select( t => t.Item2 );
        }
    }
}
