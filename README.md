# Murph.AutoFixture.AutoReadOnlyCollectionProperties #

This is an extension for the [AutoFixture](https://github.com/AutoFixture/AutoFixture/)
library that implements behavior during value generation for automatically filling
`ICollection<T>` properties that have no setter. This extends AutoFixture's property
auto-filling behavior to work on read-only properties that expose a type derived
from `ICollection<T>`, by calling the collection's `Add()` method.

## Usage ##

After creating a fixture, customize the fixture with an instance of
`AutoReadOnlyCollectionPropertiesCustomization`:

    var fixture = new Fixture.Customize( new AutoReadOnlyCollectionPropertiesCustomization() );

The behavior can be disabled for a specific member of a given type in a similar manner
to disabling AutoFixture's auto-properties behavior for a specific member. However, AutoFixture's
built-in `Without()` method does not work for read-only properties, so an extension method
named `WithoutEx()` is provided to perform the same toggling for read-only members:

    fixture.Customize< Sometype >( composer => composer.WithoutEx( st => st.SomeCollection ) );
